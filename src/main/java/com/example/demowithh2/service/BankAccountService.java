package com.example.demowithh2.service;

import com.example.demowithh2.domain.BankAccount;
import com.example.demowithh2.domain.Transfers;
import com.example.demowithh2.repo.BankAccountRepo;
import com.example.demowithh2.repo.TransferRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
public class BankAccountService {


    @Autowired
    private BankAccountRepo bankAccountRepo;

    @Autowired
    private TransferRepo transferRepo;

    @Autowired
    private TransferService transferService;


    @Transactional()
    public void transfer(int amount){
        BankAccount person1Account = bankAccountRepo.findById(1).get();
        person1Account.setBalance(person1Account.getBalance()-amount);
        bankAccountRepo.save(person1Account);

        BankAccount person2Account = bankAccountRepo.findById(2).get();
        person2Account.setBalance(person2Account.getBalance()+amount);
        bankAccountRepo.save(person2Account);

//        try{
//
//        }

        createTransferRecord();
    }

//    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void createTransferRecord(){
        transferService.createTransferRecordService();
//        Transfers transfers = Transfers.builder()
//                .account1(1)
//                .account2(2)
//                .build();
//        transferRepo.save(transfers);


    }

}
