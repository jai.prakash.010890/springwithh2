package com.example.demowithh2.service;

//import com.example.demowithh2.aop.TaskException;
import com.example.demowithh2.repo.ProductRepo;
import com.example.demowithh2.domain.ProductsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

   @Autowired
   private ProductRepo productRepo;

   public List<ProductsEntity> getAllProducts(){

//       productRepo.findAll(); // read
//       productRepo.save() //create
//       productRepo.delete();// delete

      List<ProductsEntity> entityList = new LinkedList<>();
       productRepo.findAll().forEach(productsEntity -> entityList.add(productsEntity));
       return entityList;
   }

   public ProductsEntity getById(Integer id){
       Optional<ProductsEntity> productsEntity = productRepo.findById(id);
       if(!productsEntity.isPresent()){
//           throw new TaskException(HttpStatus.NOT_FOUND, "Item not found");
       }
       return productsEntity.get();
   }

   public void findByName(){
//       List<ProductsEntity> productsEntity =  productRepo.findByColorIgnoreCase("Red");
//       System.out.println(productsEntity);
   }

   public ProductsEntity saveAProduct(ProductsEntity request){
      return productRepo.save(request);
   }
}
