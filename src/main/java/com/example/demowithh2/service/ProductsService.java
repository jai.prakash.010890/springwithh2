package com.example.demowithh2.service;

import com.example.demowithh2.domain.ProductsEntity;
import com.example.demowithh2.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductsService {

    @Autowired
    private ProductRepository productRepository;

    public List<ProductsEntity> findAllPaging(int pageidx, int pageSize){


        Pageable page = PageRequest.of(pageidx,pageSize, Sort.Direction.DESC,"productname");
        Iterable<ProductsEntity> productsEntities= productRepository.findAll(page);

        List<ProductsEntity> lst = new ArrayList<>();
//        Pageable page= PageRequest.of(pageidx,pageSize);
//
//
//        Iterable<ProductsEntity> productsEntities= productRepository.findAll(page);

        productsEntities.forEach(p-> lst.add(p));
        return lst;
    }

    public void testFindPagingAndSorting(){
        Pageable page = PageRequest.of(0,2, Sort.Direction.DESC,"productname");
        productRepository.findAll(page).forEach(p-> System.out.println(p));
    }

}
