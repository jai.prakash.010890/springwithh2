package com.example.demowithh2.service;

import com.example.demowithh2.domain.Transfers;
import com.example.demowithh2.repo.TransferRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransferService {


    @Autowired
    private TransferRepo transferRepo;


    public void createTransferRecordService(){
        Transfers transfers = Transfers.builder()
                .account1(1)
                .account2(2)
                .build();
        transferRepo.save(transfers);
        throw new RuntimeException();
    }
}
