package com.example.demowithh2.base.step1;

import com.example.demowithh2.base.step1.Rule;
import com.example.demowithh2.base.step2.RuleDataContainer;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidatorContext;

public class RequiredValidator implements Rule {
    @Override
    public boolean evaluate(String value, ConstraintValidatorContext context, RuleDataContainer ruleDataContainer) {
        if (ruleDataContainer.isRequired() && StringUtils.isBlank(value)) {
            context.buildConstraintViolationWithTemplate("required").addConstraintViolation();
            return false;
        }
        return true;
    }
}
