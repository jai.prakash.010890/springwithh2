package com.example.demowithh2.base.step1;

import com.example.demowithh2.base.step1.Rule;
import com.example.demowithh2.base.step2.RuleDataContainer;

import javax.validation.ConstraintValidatorContext;

public class LengthValidator implements Rule {
    @Override
    public boolean evaluate(String value, ConstraintValidatorContext context, RuleDataContainer ruleDataContainer) {
        if((ruleDataContainer.getMinLength() == null && ruleDataContainer.getMaxLength() == null) || value == null){
            return true;

        }

        if(value.length() < ruleDataContainer.getMinLength() ||
          value.length() > ruleDataContainer.getMaxLength()){
            context.buildConstraintViolationWithTemplate(String.format("Invalid length", ruleDataContainer.getMinLength(), ruleDataContainer.getMaxLength()));
            return false;
        }

        return true;
    }
}
