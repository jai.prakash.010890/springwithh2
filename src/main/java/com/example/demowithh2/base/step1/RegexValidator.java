package com.example.demowithh2.base.step1;

import com.example.demowithh2.base.step1.Rule;
import com.example.demowithh2.base.step2.RuleDataContainer;

import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexValidator implements Rule {
    @Override
    public boolean evaluate(String value, ConstraintValidatorContext context, RuleDataContainer ruleDataContainer) {
        Pattern pattern = Pattern.compile(value);
        Matcher matcher = pattern.matcher(value);
        try{
            if(matcher.matches())
                return true;

        }catch(Exception ex){
            return false;
        }
        return false;
    }
}
