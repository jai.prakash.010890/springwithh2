package com.example.demowithh2.base.step1;

import com.example.demowithh2.base.step2.RuleDataContainer;

import javax.validation.ConstraintValidatorContext;

public interface Rule {
    boolean evaluate(String value, ConstraintValidatorContext context, RuleDataContainer ruleDataContainer);
}
