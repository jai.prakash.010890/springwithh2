package com.example.demowithh2.base.step2;

import com.example.demowithh2.base.step3.ClientAppValidation;

import javax.validation.ConstraintValidatorContext;

public class ClientAppValidator extends BaseValidator<ClientAppValidation, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return super.isValid(value, constraintValidatorContext);
    }

    @Override
    public void initialize(ClientAppValidation clientAppValidation) {
        ruleDataContainer.setRequired(clientAppValidation.isRequired());
        ruleDataContainer.setMinLength(ruleDataContainer.getMinLength());
//        super.initialize(clientAppValidation);
//        super.initialize(clientAppValidation);
    }
}
