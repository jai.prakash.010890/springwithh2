package com.example.demowithh2.base.step2;

import com.example.demowithh2.base.step1.LengthValidator;
import com.example.demowithh2.base.step1.RegexValidator;
import com.example.demowithh2.base.step1.RequiredValidator;
import com.example.demowithh2.base.step1.Rule;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;

@Setter
abstract class BaseValidator<A extends Annotation, T> implements ConstraintValidator<A,T> {

    private static List<Rule> ruleList = new LinkedList<>();

    protected RuleDataContainer ruleDataContainer = new RuleDataContainer();

    static {
        ruleList.add(new RequiredValidator());
        ruleList.add(new LengthValidator());
        ruleList.add(new RegexValidator());
    }

    protected boolean isValid(String value, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        if(StringUtils.isEmpty(value)){
            context.buildConstraintViolationWithTemplate("Empty field").addConstraintViolation();
            return false;
        }
        for(Rule rule: ruleList){
            if(!rule.evaluate(value, context, ruleDataContainer)){
                return false;
            }
        }
        return true;
    }
}
