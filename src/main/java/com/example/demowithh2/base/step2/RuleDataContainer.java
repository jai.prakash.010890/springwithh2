package com.example.demowithh2.base.step2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RuleDataContainer {

    private String message;
    private boolean isRequired;
    private String regex;
    private Integer maxLength;
    private Integer minLength;
}
