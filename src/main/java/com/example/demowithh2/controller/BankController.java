package com.example.demowithh2.controller;

import com.example.demowithh2.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BankController {


    @Value("${time.idle}")
    private int idel;

    @Autowired
    BankAccountService bankAccountService;

    @GetMapping("/transfer")
    public ResponseEntity<String> transfer(){
        bankAccountService.transfer(200);
        return new ResponseEntity<>("TRANSFERRED", HttpStatus.OK);
    }
}
