package com.example.demowithh2.controller;

import com.example.demowithh2.configuration.Converter;
import com.example.demowithh2.model.ProductsModel;
import com.example.demowithh2.service.ProductService;
import com.example.demowithh2.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
public class ProControllerWithPaging {

    @Autowired
    private ProductsService productsService;

    @Autowired
    private Converter converter;



    @GetMapping("/pages")
    public ResponseEntity<List<ProductsModel>> paging(@RequestParam("pageidx") int pageidx,
                                                      @RequestParam("pagesize") int pagesize){
        List<ProductsModel> lst = new LinkedList<>();
        productsService.findAllPaging(pageidx-1, pagesize).forEach(
                entity -> lst.add(converter.convertToModel(entity)));
        return new ResponseEntity<>(lst, HttpStatus.OK);
    }

}
