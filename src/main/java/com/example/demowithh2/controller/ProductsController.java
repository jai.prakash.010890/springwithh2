package com.example.demowithh2.controller;

import com.example.demowithh2.aop.AspectConfig;
import com.example.demowithh2.domain.ProductsEntity;
import com.example.demowithh2.model.CustomModel;
import com.example.demowithh2.model.Item;
import com.example.demowithh2.model.ProductsModel;
import com.example.demowithh2.configuration.Converter;
import com.example.demowithh2.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
public class ProductsController {

//    private Logger log = LoggerFactory.getLogger(ProductsController.class);
    @Autowired
    private ProductService productService;

    @Autowired
    private Converter converter;

    @GetMapping("/all")
    public ResponseEntity<List<ProductsModel>> getAllProducts() {
        List<ProductsModel> lst = new LinkedList<>();

//      List<ProductsEntity> list = productService.getAllProducts();

        productService.getAllProducts().forEach(
                entity -> lst.add(converter.convertToModel(entity)));
        return new ResponseEntity<>(lst, HttpStatus.OK);
    }

    @PostMapping(value = "/saveProduct", produces = {"application/json"}, consumes = {
            "application/json"})
    ResponseEntity<ProductsModel> save(@Valid @RequestBody ProductsModel request) {
        ProductsModel response = converter.convertToModel(productService.saveAProduct(converter.convertToEntity(request)));
        return new ResponseEntity<>(response, HttpStatus.OK);


    }

    @PostMapping(value = "/testcustom")
    ResponseEntity<String> testString(@Valid @RequestBody Item model) {
        return new ResponseEntity<>("Hi", HttpStatus.OK);
    }

    @GetMapping("/authorize")
    ResponseEntity<String> authorize() {

        productService.findByName();
        return new ResponseEntity<>("Hello", HttpStatus.OK);
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<ProductsModel> findById(@PathVariable Integer id) {
        ProductsEntity entity = productService.getById(id);
        ProductsModel productsModel = converter.convertToModel(entity);
        return new ResponseEntity<>(productsModel, HttpStatus.OK);
    }


//   @GetMapping("/getByName")
//   public ResponseEntity<ProductsModel> findByIdq(@RequestParam("name") Integer id){
}
