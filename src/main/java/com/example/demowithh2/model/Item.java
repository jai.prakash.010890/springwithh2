package com.example.demowithh2.model;

import com.example.demowithh2.annotations.QualityValidation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Item {

    private String name;

    @QualityValidation
    private String quality;
}
