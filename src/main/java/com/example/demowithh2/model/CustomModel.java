package com.example.demowithh2.model;

import com.example.demowithh2.base.step3.ClientAppValidation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomModel {

    private String name;

    private Integer age;

    @NotNull
    @ClientAppValidation(isRequired = true, message = "please check address field", minLength = 5)
    private String address;
}
