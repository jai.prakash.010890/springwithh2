package com.example.demowithh2.model;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ProductsModel {
   private int productid;
   private String productname;

   @NotNull
   private String color;
   private int quantity;
}
