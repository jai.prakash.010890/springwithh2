package com.example.demowithh2.repo;

import com.example.demowithh2.domain.BankAccount;
import com.example.demowithh2.domain.ProductsEntity;
import org.springframework.data.repository.CrudRepository;

public interface BankAccountRepo extends CrudRepository<BankAccount, Integer> {
}
