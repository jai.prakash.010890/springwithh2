package com.example.demowithh2.repo;

import com.example.demowithh2.domain.Student;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student, Long> {

    // if we want partial columns then use select statement
    // "Student" is class name not table name
    @Query("from Student")
    List<Student> findAllStudents();

    // 3
    @Query("select st.firstName,st.lastName from Student st")
    List<Object[]> findAllStudentsPartialData();

    // 2
    @Query("from Student where firstName=:firstName")
    List<Student> findAllStudentsByFirstName(@Param("firstName") String firstName);

    // If we want pagination then we just need to pass Pageable
    // it is JPQL so we use Entity class
    @Query("from Student")
    List<Student> findAllStudents(Pageable pageable);


    @Modifying
    @Query("delete from Student where firstName = :firstName")
    void deleteStudentsByFirstName(@Param("firstName") String firstName);


    // Native queries please make sure that table name and column names are as per the table not as per the entity class
    @Query(value = "select * from student where fname=:firstName", nativeQuery = true)
    List<Student> findByFirstNQ(@Param("firstName") String firstName);
}
