package com.example.demowithh2.repo;

import com.example.demowithh2.domain.Transfers;
import org.springframework.data.repository.CrudRepository;

public interface TransferRepo extends CrudRepository<Transfers, Integer> {
}
