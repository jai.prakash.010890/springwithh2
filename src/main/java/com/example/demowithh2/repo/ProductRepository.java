package com.example.demowithh2.repo;

import com.example.demowithh2.domain.ProductsEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepository extends PagingAndSortingRepository<ProductsEntity, Integer> {
}
