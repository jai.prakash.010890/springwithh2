package com.example.demowithh2.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class AspectConfig {

    /**
     * Logging
     * Exception Handling
     * Time taken
     */

    private Logger log = LoggerFactory.getLogger(AspectConfig.class);


    /**
     * Joint point is a method in execution
     * <p>
     * com.example.demowithh2.controller.*.*(..) is the joint point
     *
     * @param joinPoint
     */

    @Before(value = "execution(* com.example.demowithh2.controller.*.*(..))")
    public void logStatementBefore(JoinPoint joinPoint) {
        log.info("Executing {}", joinPoint);
    }

    @After(value = "execution(* com.example.demowithh2.controller.*.*(..))")
    public void logStatementAfter(JoinPoint joinPoint) {
        log.info("Complete execution {}", joinPoint);
    }


    /**
     * Before and after will run only once but @Around will run until method is not finished
     *
     * @param joinPoint
     */

    @Around(value = "execution(* com.example.demowithh2.service.*.*(..))")
    public Object taskHandler(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            Object obj = joinPoint.proceed();
            return obj;
        } catch (TaskException exception) {
            log.info("TaskException StatusCode {}", exception.getHttpStatus().value());
            log.info("TaskException Message {}", exception.getMessage());
        }
        return null;
    }

    @Around(value = "execution(* com.example.demowithh2.service.*.*(..))")
    public Object timeTracker(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();


        try {
            Object obj = joinPoint.proceed();
            long timeTaken = System.currentTimeMillis() - startTime;
            log.info("Time taken by {} is {}", joinPoint, timeTaken);
            return obj;
        } catch (TaskException exception) {
            log.info("TaskException StatusCode {}", exception.getHttpStatus().value());
            log.info("TaskException Message {}", exception.getMessage());
        }
        return null;
    }
}
