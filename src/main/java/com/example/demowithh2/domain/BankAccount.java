package com.example.demowithh2.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "accounts")
public class BankAccount {

    @Id
    private int accno;


    private String name;


    private String city;
    private int balance;
}
