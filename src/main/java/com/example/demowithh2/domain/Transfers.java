package com.example.demowithh2.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
@Builder
@ToString
public class Transfers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int transferid;

    private int account1;

    private int account2;
}
