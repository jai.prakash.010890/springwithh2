package com.example.demowithh2.annotations;

import com.example.demowithh2.domain.ProductsEntity;
import com.example.demowithh2.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class QualityValidator implements ConstraintValidator<QualityValidation, String> {

    @Autowired
    private ProductService productService;

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        List<ProductsEntity> lst =  productService.getAllProducts();
        List list = Arrays.asList(new String[]{"GOOD", "BAD", "EXCELLENT"});
        return list.contains(s);
    }
}
